# FROM openjdk:8-jdk-alpine
FROM anapsix/alpine-java

# add files to image
COPY bb-auth-1.0-SNAPSHOT.jar auth.jar
COPY bb-credentials-1.0-SNAPSHOT.jar credentials.jar
COPY chores-ui-frontend-1.0-SNAPSHOT.jar fems.jar
COPY bb-child-crud-1.0-SNAPSHOT.jar child-crud.jar
COPY bb-child-parent-crud-1.0-SNAPSHOT.jar child-parent-crud.jar
COPY bb-chore-crud-1.0-SNAPSHOT.jar chore-crud.jar
COPY chores-ui-0.0.1.jar ui.jar
COPY bb-chore-parent-crud-1.0-SNAPSHOT.jar chore-parent-crud.jar
COPY bb-chore-child-crud-1.0-SNAPSHOT.jar chore-child-crud.jar
COPY start.sh .

# start on run
CMD ["bash", "start.sh"]
