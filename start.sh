java -jar ui.jar &
java -jar credentials.jar &
java -jar auth.jar &
java -jar fems.jar &
java -jar child-crud.jar &
java -jar child-parent-crud.jar &
java -jar chore-crud.jar &
java -jar chore-child-crud.jar &
java -jar chore-parent-crud.jar
